<?php

/**
 * @file
 * 
 * Developed by Gabor Szanto.
 *  hello@szantogabor.com
 *  http://szantogabor.com
 */

$plugin = array(
  'title' => t('Design Image search form'),
  'description' => t('A specialized search form designed for image searching.'),
  'category' => t('Design'),
  'edit form' => 'design_image_search_form_edit_form',
  'render callback' => 'design_image_search_form_render',
  //'admin info' => 'design_image_search_form_admin_info',
  'defaults' => array(
    'get_defaults' => FALSE,
    'ajax_id' => '',
  ),
);

function design_image_search_form_edit_form($form, &$form_state) {
  //$settings = ctools_custom_content_type_get_conf($form_state['subtype'], $form_state['conf']);
  //$form_state['settings'] = $settings;

  $conf = $form_state['conf'];
  $form['get_defaults'] = array(
    '#type' => 'checkbox',
    '#title' => t('Get defaults'),
    '#description' => t('Get default value from loaded taxonomy term'),
    '#default_value' => $conf['get_defaults'],
  );

  return $form;
}

function design_image_search_form_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
     $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }

  $form_state['conf']['ajax_id'] = md5(time());
}

function design_image_search_form_render($subtype, $conf, $panel_args, $context) {
  $output = drupal_get_form('design_image_search_search_form', $conf);
  $block = new stdClass();
  $block->module = 'design_image_search';
  $block->delta = $subtype;
  $block->title = '';
  $block->content = $output;
  return $block;
}
